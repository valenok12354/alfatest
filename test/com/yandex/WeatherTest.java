package com.yandex;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Assert;
import org.junit.Test;
import java.util.*;


public class WeatherTest {
    private static HttpResponse<JsonNode> connection() throws UnirestException {
        return Unirest.get("https://api.weather.yandex.ru/v1/forecast?lat=55.753215&lon=37.622504&limit=2")
                .header("X-Yandex-API-Key", "d702130d-8d69-48fa-bdd0-76c033f6aa2d").asJson();

    }

    private class Responce {
        private Fact fact;
        private Info info;
        private List<Forecasts> forecasts;

    }

    private class Fact {
        private String season;
    }

    private class Info {
        private double lon;
        private Tzinfo tzinfo;
        private String url;
        private double lat;
    }

    private class Tzinfo {
        private int offset;
        private String dst;
        private String name;
        private String abbr;
    }

    private class Forecasts {
        private String date;
        private String rise_begin;
        private String sunrise;
        private ArrayList<Map<String, Object>> hours;
        private int week;
        private String moon_text;
        private int moon_code;
    }


    @Test
    public void lattitude() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals(55.753215, resp.info.lat, 0.05);
    }

    @Test
    public void longitude() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals(37.622504, resp.info.lon, 0.05);
    }

    @Test
    public void offset() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals(10800, resp.info.tzinfo.offset);
    }

    @Test
    public void name() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals("Europe/Moscow", resp.info.tzinfo.name);
    }

    @Test
    public void abbr() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals("MSK", resp.info.tzinfo.abbr);
    }

    @Test
    public void dst() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals("false", resp.info.tzinfo.dst);
    }

    @Test
    public void url() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals("https://yandex.ru/pogoda/?lat=55.753215&lon=37.622504", resp.info.url);
    }

    @Test
    public void daysOfTheForecast() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals(2, resp.forecasts.size());
    }

    @Test
    public void season() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        Assert.assertEquals("summer", resp.fact.season);
    }

    @Test
    public void moonCodeConverter() throws UnirestException {
        Gson g = new Gson();
        Responce resp = g.fromJson(connection().getBody().toString(), Responce.class);
        int code = resp.forecasts.get(1).moon_code;
        switch ((code == 0) ? 0 :
                (1 <= code && code <= 3) ? 1 :
                        (code == 4) ? 2 :
                                (5 <= code && code <= 7) ? 3 :
                                        (code == 8) ? 4 :
                                                (9 <= code && code <= 11) ? 5 :
                                                        (code == 12) ? 6 : 7
        ) {
            case 0:
                Assert.assertEquals("full-moon", resp.forecasts.get(1).moon_text);
                break;
            case 1:
                Assert.assertEquals("decreasing-moon", resp.forecasts.get(1).moon_text);
                break;
            case 2:
                Assert.assertEquals("last-quarter", resp.forecasts.get(1).moon_text);
                break;
            case 3:
                Assert.assertEquals("decreasing-moon", resp.forecasts.get(1).moon_text);
                break;
            case 4:
                Assert.assertEquals("new-moon", resp.forecasts.get(1).moon_text);
                break;
            case 5:
                Assert.assertEquals("growing-moon", resp.forecasts.get(1).moon_text);
                break;
            case 6:
                Assert.assertEquals("first-quarter", resp.forecasts.get(1).moon_text);
                break;
            case 7:
                Assert.assertEquals("growing-moon", resp.forecasts.get(1).moon_text);
                break;

        }
    }
}
